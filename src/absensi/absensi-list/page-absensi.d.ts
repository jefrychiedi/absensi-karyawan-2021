export interface IAbsensiData {
  name: string,
  date: string,
  status: string,
  keterangan: string?,
  id: number
}