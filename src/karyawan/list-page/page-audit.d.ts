export interface IUserData {
  name: string,
  age: number,
  role: string?,
  email: string?,
  id: number
}