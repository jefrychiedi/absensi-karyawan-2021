import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { PageAbsensiComponent } from './absensi/absensi-list/page-absensi.component';
import { PageCreateAbsensi } from './absensi/absensi-page/page-create-absensi.component';
import { PageUpdateAbsensi } from './absensi/absensi-update/page-update-absensi.component';
import { BaseMainComponent } from './base/base-main.component';
import { PageHomeComponent } from './base/page-home.component';
import { PageCreateComponent } from './karyawan/create-page/page-create.component';
import { PageAuditComponent } from './karyawan/list-page/page-audit.component';
import { PageUpdateComponent } from './karyawan/update-page/page-update.component';

import { PageNotFoundComponent } from './not-found/page-not-found.component';

function App() {
  return (
    <Router>
      <Switch>

        <Route exact path='/'>
          <BaseMainComponent>
            <PageHomeComponent />
          </BaseMainComponent>
        </Route>

        <Route exact path='/absensi'>
          <BaseMainComponent>
            <PageCreateAbsensi />
          </BaseMainComponent>
        </Route>

        <Route exact path='/absensi/data'>
          <BaseMainComponent>
            <PageAbsensiComponent />
          </BaseMainComponent>
        </Route>

        <Route exact path='/absensi/data/update/:id'>
          <BaseMainComponent>
            <PageUpdateAbsensi />
          </BaseMainComponent>
        </Route>

        <Route exact path='/karyawan/data'>
          <BaseMainComponent>
            <PageAuditComponent />
          </BaseMainComponent>
        </Route>

        <Route exact path='/karyawan/create'>
          <BaseMainComponent>
            <PageCreateComponent />
          </BaseMainComponent>
        </Route>

        <Route exact path='/karyawan/data/update/:id'>
          <BaseMainComponent>
            <PageUpdateComponent />
          </BaseMainComponent>
        </Route>

        <Route exact path='/404'> 
            <PageNotFoundComponent />
        </Route>
        
        <Route exact path='*'>
          <Redirect to='/404' />
          </Route>

      </Switch>
    </Router>
  );
}

export default App;
